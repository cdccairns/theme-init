# theme-init

## Usage

This repository is intended to act as a source directory for the development of a custom theme. Its contents should be processed and compiled to a distribution directory using a task runner (e.g. [Gulp](https://gulpjs.com/) or [Webpack](https://webpack.js.org/)).

## Installation

> The following instructions presume the existence of a project subdirectory into which the repository can be cloned, and an instance of `package.json` located in a project root directory.

In your project subdirectory, run `$ git clone <repository> <directory>` and `$ cd <directory> rm -rf .git` to clone the repository and delete its commit history.

### Font Awesome

Themes implementing [Font Awesome](https://www.npmjs.com/package/@fortawesome/fontawesome-free) can make use of the provided import stylesheet at `scss/font-awesome.scss`. To install Font Awesome, run `$ npm install --save @fortawesome/fontawesome-free`, confirming afterwards that the path to the package directory (`node_modules`) is configured correctly in the import stylesheet.

If your theme does not require Font Awesome, run `$ rm scss/font-awesome.scss` to delete the import stylesheet.

## Roadmap

* Document structure and configuration.
* Add default `list` and `table` styles.
* Implement `grid` object.
* Deprecate `@import` at-rules in favour of `@use` and `@forward`. (See [The Next-Generation Sass Module System: Draft 6](https://github.com/sass/sass/blob/master/accepted/module-system.md)).

## References

* [Xfive: ITCSS: Scalable and Maintainable CSS Architecture](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/) by Lubos Kmetko
* [CSS Wizardry: More Transparent UI Code with Namespaces](https://csswizardry.com/2015/03/more-transparent-ui-code-with-namespaces/)
* [GOV.UK Frontend](https://github.com/alphagov/govuk-frontend/tree/master/src/govuk) by the GOV.UK Design System team
* [Grid By Example](https://gridbyexample.com) (see [Resources](https://gridbyexample.com/resources/))
* [Every Layout](https://every-layout.dev/)
* [A List Apart: Axiomatic CSS and Lobotomized Owls](https://alistapart.com/article/axiomatic-css-and-lobotomized-owls/) by Heydon Pickering
* [Inclusive Components](https://inclusive-components.design/)
* [The State of CSS 2019](https://2019.stateofcss.com/)
